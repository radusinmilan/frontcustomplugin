Ext.define('Shopware.apps.SwagProductBasic.view.list.Product', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.product-listing-grid',
    region: 'center',

    configure: function() {
        return {

             // This will show only name column, defaults it all columns
            columns: {
                name: this.createNameColumn, // Change name of header of column
                description: { flex: 3 }, //Sirina flex 3
                active: { width: 60, flex: 0 }  // Active colona
            },
            detailWindow: 'Shopware.apps.SwagProductBasic.view.detail.Window'
        };
    },
    createNameColumn: function(model, column) {
        column.header = 'Product name';
        return column;
    }
});